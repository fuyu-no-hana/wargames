`ls -la`

`cat .bash_logout`

`cat .bashrc`

`cat .profile`

`cd /`

`ls`

`cat README.txt`

`man find`

`find . -user bandit7 -group bandit6 -size 33c`

Output: a bunch of "permission denied"s plus one non-error line consisting of `./var/lib/dpkg/info/bandit7.password`

`cat ./var/lib/dpkg/info/bandit7.password`

Found HKBPTKQnIay4Fw76bEy8PVxKEDQRKTzs

`exit`

`ssh bandit.labs.overthewire.org -p 2220 -l bandit7`

Enter HKBPTKQnIay4Fw76bEy8PVxKEDQRKTzs at prompt

Success!