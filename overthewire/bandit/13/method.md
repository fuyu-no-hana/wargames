`ls`

`mkdir /tmp/skye`

`cp data.txt /tmp/skye/`

`cd /tmp/skye`

`ls`

`cat data.txt`

`man tar`

`man xxd`

`xxd -r data.txt data2.bin`

`file data2.bin`

`man gzip`

`gzip -d data2.bin`

`gzip -f -d data2.bin`

`gunzip -f data2.bin`

`gunzip --suffix .bin data2.bin`

`ls`

`cat data2`

`file data2`

`man bzip2`

`bunzip2 data2`

`ls`

`file data2.out`

`mv data2.out data4.bin`

`gunzip --suffix .bin data4.bin`

`ls`

`file data4`

`tar xvf data4`

`file data5.bin`

`tar xvf data5.bin`

`file data6.bin`

`bunzip2 data6.bin`

`file data6.bin.out`

`tar xvf data6.bin.out`

`file data8.bin`

`mv data8.bin data9.bin`

`gunzip --suffix .bin data9.bin`

`ls`

`file data9`

`cat data9`

Found 8ZjyCRiBWFYkneahHwxCv3wb2a1ORpYL

`exit`

`ssh bandit.labs.overthewire.org -p 2220 -l bandit13`

Enter 8ZjyCRiBWFYkneahHwxCv3wb2a1ORpYL at prompt

Success!